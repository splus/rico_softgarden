$(document).ready(function () {
  // Disable submit of filter form
  $('#jobs-filter').submit(function (event) {
    event.preventDefault()
  })
  // Toggles jobs, depending on the selected filter options.
  $('.change-listener').change(function () {
    var selectedCareerLevel = parseInt($('#jobs-filter-career-levels option:selected').val())
    var selectedLocation = parseInt($('#jobs-filter-locations option:selected').val())
    hideAllJobs($('.job'))
    $('.job').each(function (i, e) {
      var hideJob = false
      var $job = $(e)
      var careerLevel = parseInt($job.data('career-levels'))
      var location = parseInt($job.data('location'))
      if (selectedCareerLevel >= 0 && !hideJob) {
        if (selectedCareerLevel !== careerLevel) {
          hideJob = true
        }
      }
      if (selectedLocation >= 0 && !hideJob) {
        if (selectedLocation !== location) {
          hideJob = true
        }
      }
      hideJob ? hideSingleJob($job) : showSingleJob($job)
    })
  })

  /**
   * Hides all jobs.
   * @param $jobs
   */
  function hideAllJobs ($jobs) {
    $jobs.each(function () {
      hideSingleJob($(this))
    })
  }

  /**
   * Hides a single job.
   * @param $job
   */
  function hideSingleJob ($job) {
    $job.hide()
  }

  /**
   * Shows a single job
   * @param $job
   */
  function showSingleJob ($job) {
    $job.show()
  }

})