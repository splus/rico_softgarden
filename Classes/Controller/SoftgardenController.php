<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Controller;

use Riconet\RicoSoftgarden\Domain\Provider\CareerLevelProvider;
use Riconet\RicoSoftgarden\Domain\Provider\JobProvider;
use Riconet\RicoSoftgarden\Domain\Provider\LocationProvider;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class SoftgardenController extends ActionController
{
    /**
     * @var JobProvider
     */
    protected $jobProvider;

    /**
     * @var LocationProvider
     */
    protected $locationProvider;

    /**
     * @var CareerLevelProvider
     */
    protected $careerLevelProvider;

    public function injectJobProvider(JobProvider $jobProvider)
    {
        $this->jobProvider = $jobProvider;
    }

    public function injectLocationProvider(LocationProvider $locationProvider)
    {
        $this->locationProvider = $locationProvider;
    }

    public function injectCareerLevelProvider(CareerLevelProvider $careerLevelProvider)
    {
        $this->careerLevelProvider = $careerLevelProvider;
    }

    public function indexAction()
    {
        $this->view->assignMultiple([
            'jobs' => $this->jobProvider->getObjects(),
            'careerLevels' => $this->careerLevelProvider->getObjects(),
            'locations' => $this->locationProvider->getObjects(),
        ]);
    }
}
