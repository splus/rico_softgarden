<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden;

class Constants
{
    const EXTENSION_KEY = 'rico_softgarden';

    const CACHE_LIFE_TIME = 21600;

    const CACHE_TAGS = ['jobs'];
}
