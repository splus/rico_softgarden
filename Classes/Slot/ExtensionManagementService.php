<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Slot;

use Riconet\RicoSoftgarden\Constants;
use Riconet\RicoSoftgarden\Domain\Service\DetailPageCacheService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ExtensionManagementService
{
    public function hasInstalledExtensions(string $extensionKey = ''): void
    {
        if (Constants::EXTENSION_KEY !== $extensionKey) {
            return;
        }
        /** @var DetailPageCacheService $detailPageCacheService */
        $detailPageCacheService = GeneralUtility::makeInstance(DetailPageCacheService::class);
        $detailPageCacheService->fetch();
    }
}
