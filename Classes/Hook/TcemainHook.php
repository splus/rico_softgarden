<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Hook;

use Riconet\RicoSoftgarden\Domain\Service\DetailPageCacheService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Exception;

class TcemainHook
{
    public function clearCachePostProc(): void
    {
        try {
            /** @var DetailPageCacheService $detailPageCacheService */
            $detailPageCacheService = GeneralUtility::makeInstance(DetailPageCacheService::class);
            $detailPageCacheService->fetch();
        } catch (Exception $exception) {
            // ignored...
        }
    }
}
