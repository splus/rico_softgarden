<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

class ArrayIndexViewHelper extends AbstractViewHelper
{
    public function initializeArguments(): void
    {
        $this->registerArgument('haystack', 'array', 'The haystack', true);
        $this->registerArgument('needle', 'mixed', 'The needle', true);
    }

    public function render(): string
    {
        return (string) static::renderStatic(
            $this->arguments,
            $this->buildRenderChildrenClosure(),
            $this->renderingContext
        );
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        foreach ($arguments['haystack'] as $key => $value) {
            if ($arguments['needle'] === $value) {
                return $key;
            }
        }

        return null;
    }
}
