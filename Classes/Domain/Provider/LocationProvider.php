<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Domain\Provider;

use Riconet\RicoSoftgarden\Constants;
use TYPO3\CMS\Core\SingletonInterface;

class LocationProvider implements SingletonInterface, CacheableProviderInterface
{
    /**
     * @var CacheProvider
     */
    protected $cacheProvider;

    /**
     * @var JobProvider
     */
    protected $jobProvider;

    public function injectJobProvider(JobProvider $jobProvider)
    {
        $this->jobProvider = $jobProvider;
    }

    public function injectCacheProvider(CacheProvider $cacheProvider)
    {
        $this->cacheProvider = $cacheProvider;
    }

    public function getObjects()
    {
        $locations = $this->cacheProvider->getEntry('job_locations');
        if (!is_array($locations)) {
            $locations = $this->extractJobLocations();
            $this->cacheProvider->setEntry(
                'job_locations',
                $locations,
                Constants::CACHE_TAGS,
                Constants::CACHE_LIFE_TIME
            );
        }

        return $locations;
    }

    private function extractJobLocations(): array
    {
        $locations = [];
        $jobs = $this->jobProvider->getObjects();
        foreach ($jobs as $job) {
            if (!isset($job->config->ProjectGeoLocationCity[0]) || !is_string($job->config->ProjectGeoLocationCity[0])
            ) {
                continue;
            }
            $location = trim($job->config->ProjectGeoLocationCity[0]) ?? null;
            if (empty($location) || in_array($location, $locations)) {
                continue;
            }
            $locations[] = $location;
        }

        return $locations;
    }
}
