<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Domain\Provider;

use Riconet\RicoSoftgarden\Constants;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CacheProvider implements SingletonInterface
{
    const DEFAULT_CACHE_LIFE_TIME = 86400;

    const IDENTIFIER = Constants::EXTENSION_KEY;

    public function getEntry(string $identifier)
    {
        $cache = $this->getCache();

        return $cache->get($identifier);
    }

    public function setEntry(
        string $identifier,
        $entry,
        array $tags = [],
        int $lifetime = self::DEFAULT_CACHE_LIFE_TIME
    ): void {
        $cache = $this->getCache();
        $this->setCacheEntry($cache, $identifier, $entry, $tags, $lifetime);
    }

    protected function getCache(): FrontendInterface
    {
        /** @var CacheManager $cacheManager */
        $cacheManager = GeneralUtility::makeInstance(CacheManager::class);

        return $cacheManager->getCache(self::IDENTIFIER);
    }

    protected function setCacheEntry(
        FrontendInterface $cache,
        string $identifier,
        $entry,
        array $tags,
        int $lifetime
    ): void {
        $cache->set($identifier, $entry, $tags, $lifetime);
    }
}
