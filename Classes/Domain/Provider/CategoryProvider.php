<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Domain\Provider;

use Riconet\RicoSoftgarden\Constants;
use TYPO3\CMS\Core\SingletonInterface;

class CategoryProvider implements SingletonInterface, CacheableProviderInterface
{
    /**
     * @var CacheProvider
     */
    protected $cacheProvider;

    /**
     * @var JobProvider
     */
    protected $jobProvider;

    public function injectJobProvider(JobProvider $jobProvider)
    {
        $this->jobProvider = $jobProvider;
    }

    public function injectCacheProvider(CacheProvider $cacheProvider)
    {
        $this->cacheProvider = $cacheProvider;
    }

    public function getObjects()
    {
        $categories = $this->cacheProvider->getEntry('job_categories');
        if (!is_array($categories)) {
            $categories = $this->extractJobCategories();
            $this->cacheProvider->setEntry(
                'job_categories',
                $categories,
                Constants::CACHE_TAGS,
                Constants::CACHE_LIFE_TIME
            );
        }

        return $categories;
    }

    private function extractJobCategories(): array
    {
        $categories = [];
        $jobs = $this->jobProvider->getObjects();
        foreach ($jobs as $job) {
            $category = trim($job->config->softgarden_industry[0]) ?? null;
            if (empty($category) || in_array($category, $categories)) {
                continue;
            }
            $categories[] = $category;
        }

        return $categories;
    }
}
