<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Domain\Provider;

use Riconet\RicoSoftgarden\Constants;
use Riconet\RicoSoftgarden\Domain\Factory\SoftgardenFactory;
use TYPO3\CMS\Core\SingletonInterface;
use stdClass;
use RuntimeException;
use Softgarden;

class JobProvider implements SingletonInterface, CacheableProviderInterface
{
    const JOBBOARDS_URL = 'v2/frontend/jobboards';

    const JOBS_URL = 'v2/frontend/jobboards/JOBBOARD_ID/jobs';

    /**
     * @var CacheProvider
     */
    protected $cacheProvider;

    public function injectCacheProvider(CacheProvider $cacheProvider)
    {
        $this->cacheProvider = $cacheProvider;
    }

    public function getObjects()
    {
        $jobs = $this->cacheProvider->getEntry('jobs');
        if (!is_array($jobs)) {
            $jobs = $this->getJobsByAPI();
            $this->cacheProvider->setEntry('jobs', $jobs, Constants::CACHE_TAGS, Constants::CACHE_LIFE_TIME);
        }

        return $jobs;
    }

    private function getJobsByAPI()
    {
        $api = SoftgardenFactory::build();
        $jobboard = $this->getJobboards($api)[0];
        $url = str_replace('JOBBOARD_ID', $jobboard->id, self::JOBS_URL);
        $jobs = $api->get($url);
        if (!$jobs instanceof stdClass) {
            throw new RuntimeException(
                "Requesting the API URL $url has failed!",
                1534493317
            );
        }

        return $jobs->results;
    }

    private function getJobboards(Softgarden $api): array
    {
        $jobboards = $api->get(self::JOBBOARDS_URL);
        if (!is_array($jobboards) || count($jobboards) <= 0) {
            throw new RuntimeException(
                'Requesting the API URL '.self::JOBBOARDS_URL.' has failed!',
                1534492888
            );
        }

        return $jobboards;
    }
}
