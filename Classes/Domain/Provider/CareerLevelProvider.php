<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Domain\Provider;

use Riconet\RicoSoftgarden\Constants;
use TYPO3\CMS\Core\SingletonInterface;

class CareerLevelProvider implements SingletonInterface, CacheableProviderInterface
{
    /**
     * @var CacheProvider
     */
    protected $cacheProvider;

    /**
     * @var JobProvider
     */
    protected $jobProvider;

    public function injectJobProvider(JobProvider $jobProvider)
    {
        $this->jobProvider = $jobProvider;
    }

    public function injectCacheProvider(CacheProvider $cacheProvider)
    {
        $this->cacheProvider = $cacheProvider;
    }

    public function getObjects()
    {
        $careerLevels = $this->cacheProvider->getEntry('job_career_levels');
        if (!is_array($careerLevels)) {
            $careerLevels = $this->extractJobCareerLevels();
            $this->cacheProvider->setEntry(
                'job_career_levels',
                $careerLevels,
                Constants::CACHE_TAGS,
                Constants::CACHE_LIFE_TIME
            );
        }

        return $careerLevels;
    }

    private function extractJobCareerLevels(): array
    {
        $careerLevels = [];
        $jobs = $this->jobProvider->getObjects();
        foreach ($jobs as $job) {
            $careerLevel = trim($job->config->softgarden_careerLevel_de[0]) ?? null;
            if (empty($careerLevel) || in_array($careerLevel, $careerLevels)) {
                continue;
            }
            $careerLevels[] = $careerLevel;
        }

        return $careerLevels;
    }
}
