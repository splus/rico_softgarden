<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Domain\Service;

use Riconet\RicoSoftgarden\Domain\Factory\SoftgardenFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use stdClass;
use RuntimeException;
use Softgarden;

/**
 * Class DetailPageCacheService.
 *
 * This service class, is used to fetch all possible detail pages of the api,
 * to store them as static html files on the server.
 */
class DetailPageCacheService
{
    const JOBBOARDS_URL = 'v2/frontend/jobboards';

    const JOBBOARD_URL = 'v2/frontend/jobboards/JOBBOARD_ID/jobs';

    const JOB_URL = 'v2/frontend/jobs';

    const PAGE_RELATIVE_PATH = 'fileadmin/softgarden';

    /**
     * @var Softgarden
     */
    private $api;

    /**
     * @var string
     */
    private $pageFileRoot = '';

    public function __construct()
    {
        $this->initialize();
    }

    private function initialize(): void
    {
        $this->api = SoftgardenFactory::build();
        $this->pageFileRoot = GeneralUtility::getFileAbsFileName(self::PAGE_RELATIVE_PATH);
    }

    public function fetch(): void
    {
        $this->flushPages();
        $this->fetchAll($this->getJobsByAPI());
    }

    private function getJobsByAPI()
    {
        $jobBoard = $this->getJobBoards($this->api)[0];
        $url = str_replace('JOBBOARD_ID', $jobBoard->id, self::JOBBOARD_URL);
        $jobs = $this->api->get($url);
        if (!$jobs instanceof stdClass) {
            throw new RuntimeException(
                "Requesting the API URL $url has failed!",
                1534493317
            );
        }

        return $jobs->results;
    }

    private function getJobBoards(Softgarden $api): array
    {
        $jobBoards = $api->get(self::JOBBOARDS_URL);
        if (!is_array($jobBoards) || count($jobBoards) <= 0) {
            throw new RuntimeException(
                'Requesting the API URL '.self::JOBBOARDS_URL.' has failed!',
                1534492888
            );
        }

        return $jobBoards;
    }

    private function fetchAll(array $jobs): void
    {
        foreach ($jobs as $job) {
            if (!$job instanceof stdClass || !property_exists($job, 'jobPostingId')) {
                continue;
            }
            $this->fetchHtml($job->jobPostingId);
        }
    }

    private function fetchHtml(int $id): void
    {
        $url = self::JOB_URL.'/'.$id;
        file_put_contents($this->pageFileRoot.'/page-'.$id.'.html', $this->api->get($url));
    }

    private function flushPages(): void
    {
        $files = scandir($this->pageFileRoot);
        if (!is_array($files) || 0 === count($files)) {
            return;
        }
        $exclude = ['.', '..'];
        foreach ($files as $file) {
            if (in_array($file, $exclude) || !file_exists($this->pageFileRoot.'/'.$file)) {
                continue;
            }
            unlink($this->pageFileRoot.'/'.$file);
        }
    }
}
