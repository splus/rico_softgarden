<?php
/*
 * This file is part of the "rico_softgarden" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 Wolf Utz <utz@riconet.de>, riconet
 */

declare(strict_types=1);

namespace Riconet\RicoSoftgarden\Domain\Factory;

use Riconet\RicoSoftgarden\Constants;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Softgarden;

class SoftgardenFactory
{
    public static function build(): Softgarden
    {
        /** @var ExtensionConfiguration $extensionConfiguration */
        $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        /** @var Softgarden $softgarden */
        $softgarden = GeneralUtility::makeInstance(
            Softgarden::class,
            [
                'appId' => $extensionConfiguration->get(Constants::EXTENSION_KEY, 'clientID')
            ]
        );

        return $softgarden;
    }
}
