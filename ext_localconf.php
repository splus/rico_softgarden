<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(
    function ($extensionKey) {
        // Autoload talentstorm-api-client library, if the system is not in composer mode.
        if (!defined('TYPO3_COMPOSER_MODE') || !TYPO3_COMPOSER_MODE) {
            @include 'phar://'.\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extensionKey)
                .'Libraries/softgarden-php-sdk.phar/vendor/autoload.php';
        }
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            "Riconet.$extensionKey",
            'Softgarden',
            [
                'Softgarden' => 'index',
            ]
        );
        if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$extensionKey])) {
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$extensionKey] = [];
        }
        if (TYPO3_MODE === 'BE') {
            // Add a slot to hook in to the extension install process.
            \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class)
                ->connect(
                    \TYPO3\CMS\Extensionmanager\Service\ExtensionManagementService::class,
                    'hasInstalledExtensions',
                    \Riconet\RicoSoftgarden\Slot\ExtensionManagementService::class,
                    'hasInstalledExtensions'
                );
            // Hook in to the clear cache action, to refetch softgarden pages.
            $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'][$extensionKey] =
                \Riconet\RicoSoftgarden\Hook\TcemainHook::class.'->clearCachePostProc';
        }
    },
    'rico_softgarden'
);
