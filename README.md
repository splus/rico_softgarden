# Softgarden - Job listing

## Dependencies
* TYPO3 9.5.X

## Description
Displays jobs fetched from the API of softgarden, using only request with basic authorization.

## How to install - not composer mode
* Get and install [composer](https://getcomposer.org/)
* Navigate to the root of the extension and run:
````bash
composer install --no-dev
````
* Activate the extension via the Extensions module.
* Include the static typoscript file.
* Install frontend dependencies using `yarn install`

## How to integrate?
Open the  Extensions module, enter the settings of ``rico_softgarden`` and set the option ``api.clientID`` to a valid value.

## API
See: http://dev.softgarden.de/career-websites-api/