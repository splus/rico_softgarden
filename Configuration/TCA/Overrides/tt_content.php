<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(
    function ($extensionKey) {
        $ll = "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf";
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            "Riconet.$extensionKey",
            'Softgarden',
            "$ll:plugin_softgarden"
        );
    },
    'rico_softgarden'
);
