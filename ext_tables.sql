#
# Table structure for 'cf_rico_softgarden'
#
CREATE TABLE cf_rico_softgarden (
    id INT(11) NOT NULL AUTO_INCREMENT,
    identifier VARCHAR(250) NOT NULL DEFAULT '',
    tag VARCHAR(250) NOT NULL DEFAULT '',
    expires INT(10) UNSIGNED DEFAULT 0 NOT NULL,
    content MEDIUMBLOB,
    PRIMARY KEY (id),
    KEY cache_id (identifier),
    KEY cache_tag (tag)
);